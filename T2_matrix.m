function T2_matrix

[~, ~,LV_inv, ~] = get_LV;

close all;

% load('LV_matrix.mat');
T2_atris_inv_old = [1 0 0; 0 0 -1; 0 1 0];
% T3_inv = [0 -1 0; 0 0 1; 1 0 0];
% T3_inv*T2_atris_inv_old
T_corr = eye(3);
T_corr = [1 0 0; 0 -1 0; 0 0 -1];
% T2_atris_inv = eye(3);
% T_corr = [-1 0 0; 0 -1 0; 0 0 -1];
T2_atris_inv = [0 1 0; 0 0 1; 1 0 0];
% T2_atris_inv = T2_atris_inv_old;
% T_corr = [0 1 0; 1 0 0; 0 0 -1];
% T_corr = T2_atris_inv_old'*T2_atris_inv;
% T_corr = [1 0 0; 0 -1 0; 0 0 -1];

figure('Units','normalized','Position',[0.1 0.1 0.7 0.7]);

ax_name_plane = {'x, ������', 'y, ����', 'z, �����'};
% ax_name_camera = {'x, ��', 'y, �����', 'z, ���'};
ax_name_image = {'Xr','Yr'};

for iCam = 1:6
    
    filepath = 'D:\Data\���_����������';
    filename = sprintf('���_����������e0.dat0x4%u_screenshot.bmp',iCam);
    im = imread(fullfile(filepath,filename));
    sbp = subplot(2,3,iCam);
    hold(sbp,'on');
    grid(sbp,'on');

%     im = permute(im,[2 1 3]);
%     im = fliplr(im);
    imshow(im);
    
    zlim(sbp,[-30 30]);
    
    x_ax = get(sbp,'xlim');
    y_ax = get(sbp,'ylim');
    z_ax = get(sbp,'zlim');
    lim_ax = [x_ax; y_ax; z_ax];
    d_lim_ax = lim_ax(:,2) - lim_ax(:,1);
    d_lim_ax = 2*repmat(min(d_lim_ax),3,1);
    
    g = 0.4;
    p0 = ceil(sum(lim_ax,2)/2);
%     p0 = [150; 175; 100];
    px = [1 0 0]';
    py = [0 1 0]';
    pz = [0 0 -1]'; % �������� ������ ��� ���������
    p = {px, py, pz};
    
   
    for iAxis = 1:3
        p_plane = p{iAxis};

%         T_corr = 
        p_cam = T2_atris_inv*LV_inv{iCam}*p_plane;
%         p_cam = [0 -1 0; ]
%     p_cam = T2_atris_inv*T_corr'*LV_inv{iCam}*p_plane;
%         p_cam = T2_atris_inv_old*LV_inv{iCam}*p_plane;

        p_cam = g*max(d_lim_ax)*p_cam;
        
%         tmp = p_cam;
%         p_cam(1) = -tmp(2);
%         p_cam(2) = tmp(3);
%         p_cam(3) = tmp(1);
        
        quiver3(p0(1),p0(2),p0(3),p_cam(1),p_cam(2),p_cam(3),0,'Color','b');%,'MaxHeadSize',0.4)
        p1 = p0 + p_cam;
        p1 = 1.05*p1;
        text(p1(1),p1(2),p1(3), sprintf(ax_name_plane{iAxis}),'Color','b')

%         p02 = [p0(1); p0(2)];
%         p_cam2(1:2) = [-p_cam(2); p_cam(3)]./p_cam(1);
%         
%         quiver(p02(1),p02(2),p_cam2(1),p_cam2(2),0,'Color','b');%,'MaxHeadSize',0.4)
%         p1 = p02 + p_cam2;
%         p1 = 1.05*p1;
%         text(p1(1),p1(2), sprintf(ax_name_plane{iAxis}),'Color','b')
        
        
    end
   
end

end