function [pointsOut, ind] = projectPointsOnSphere(points, h)
%#codegen

% R = 10;
% h = 2;
% points = [1 -1 1; 1 -1e-3 1; 1 1 1; 3 5 6]';

R = 6371*1e+3; % ������ �����
c = h + R;

ax = points(1,:);
ay = points(2,:);
az = points(3,:);
A = ax.^2./ay.^2 + az.^2./ay.^2 + 1;
B = 2*c;
C = - R^2 + c^2;
D = B.^2 - 4.*A.*C;

ind = (D>=0 & points(2,:)<0);

y_s = (-B+sqrt(D(ind)))./(2*A(ind)); %%%%%
x_s = ax(ind).*y_s./ay(ind);
z_s = az(ind).*y_s./ay(ind);

pointsOut = zeros(size(points));
pointsOut = [x_s; y_s; z_s];

end