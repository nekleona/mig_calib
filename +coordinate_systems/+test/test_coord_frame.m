function test_coord_frame

% ���� ��������� �� ������
% Z:\Data\approaches\plane_visible\ses2file0minute0-30.dat0x41_20500-22000_712-1255.bmp
load('navigation.mat')

x_norm = rand(3)';

% ����� ������ � �������� ������
iCam = 1;
% �������� � 1 ������
iFrame = 1;
% ������� 'selex'
cam_type = 'selex';

%% ��������� (��� ���� � ���)
LV = navigation.biLV_Matrix.Data(:,:,iFrame);
MGD = navigation.biMGD_Matrix.Data(:,:,iFrame);

T2 = [0 -1 0; 0 0 1; 1 0 0];

x_matrix = T2*LV*MGD*x_norm;

%% ��������� �� ������ ������� �� ���������

LV_novg = coordinate_systems.matrix.LV(iCam);

biVector2 = navigation.biVector2.Data(:,:,iFrame);
yaw = biVector2(1);
pitch = biVector2(2);
roll = biVector2(3);
NPSK = coordinate_systems.matrix.NPSK(yaw, pitch, roll);

T2_novg = coordinate_systems.matrix.T2(cam_type);

T3 = [0 0 1; -1 0 0; 0 1 0];
x_matrix_novg = T3'*T2_novg'*LV_novg'*NPSK'*x_norm;

%% ��������

err = max(max(x_matrix-x_matrix_novg));

fprintf('err = %.4f\n',err);

end