function test_horiz

h = 5175; %%%
R = 6371*1e+3; % ������ �����
c = h + R;

tmp = R^2/c^2;

ay = -sqrt(1-tmp);

N = 50;

% 0.9 < R/c
ax = -0.9:1/N:0.9;
% ax = -sqrt(tmp):1/N:sqrt(tmp);
ax2 = ax.^2;
% az2 = 1 - ax2 - ay2;
az2 = tmp - ax2;
az_plus = sqrt(az2);
az_minus = -az_plus;

points = [[ax ax]', repmat(ay,2*numel(ax),1), [az_plus az_minus]'];
% points = [[az_plus az_minus]', [ax ax]',repmat(ay,2*numel(ax),1)];

plot3(points(:,1),points(:,2),points(:,3),'b*');
grid on;
xlim([-1 1])
ylim([-1 1])
zlim([-1 1])

end