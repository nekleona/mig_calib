function dcm = angle2dcm_codegen(r1, r2, r3, rotationSequence)
% dcm = angle2dcm(r1, r2, r3)
% dcm = angle2dcm(r1, r2, r3, rotationSequence)
% size(ri) = [1 1];
%#codegen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TEST
% r1 = rand;
% r2 = rand;
% r3 = rand;
% r1 = pi/4;
% r2 = pi/2;
% r3 = pi;
% rotationSequence = 'ZYX';
% A = angle2dcm(r1, r2, r3, rotationSequence);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

narginchk(3, 4);
if nargin < 4
    rotationSequence = 'ZYX';
end

angles = [r1(:), r2(:), r3(:)];
assert(numel(angles)==3);

sang = sin(angles);
cang = cos(angles);

dcm = eye(3);
for i = 3:-1:1
    switch rotationSequence(i)
        case 'X'
            Ax = [  1       0         0;...
                    0       cang(i)   sang(i);...
                    0      -sang(i)   cang(i)];
            dcm = dcm*Ax;
        case 'Y'
            Ay = [  cang(i)     0   -sang(i);...
                    0           1    0;...
                    sang(i)     0    cang(i)];
            dcm = dcm*Ay;
        case 'Z'
            Az = [  cang(i)     sang(i)     0;...
                   -sang(i)     cang(i)     0;...
                    0           0           1];
            dcm = dcm*Az;
       otherwise
          error('Wrong rotation sequence');
    end
end
end

