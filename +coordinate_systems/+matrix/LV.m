function val = LV(i_oar)

sq = sqrt(2)/2;

switch i_oar
    case 1
        val = [sq sq 0; sq -sq 0; 0 0 -1];
    case 2
        val = [0 sq -sq; 0 sq sq; 1 0 0];
    case 3
        val = [0 sq sq; 0 sq -sq; -1 0 0];
    case 4
        val = [-sq sq 0; sq sq 0; 0 0 -1];
    case 5
        val = [sq sq 0; -sq sq 0; 0 0 1];
    case 6
        val = [-sq -sq 0; -sq sq 0; 0 0 -1];
    otherwise
        error('����� ������ ����� ���� �� 1 �� 6');
end

end