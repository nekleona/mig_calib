function val = T2(type)

switch type
    case 'orion'
        val = eye(3);
    case 'selex'
        val = eye(3);
    case 'atris'
        val = [1 0 0; 0 0 1; 0 -1 0];
    otherwise
        error('��������� ���� �������: orion, selex, atris');
end