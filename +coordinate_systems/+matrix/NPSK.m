function val = NPSK(yaw, pitch, roll)
% �� ���� ���� [����, ������, ����], ����� ������������ [��������, ������,
% ����], ��� ��������=-����

val = zeros(3);
angles = [roll pitch -yaw];
c = cos(angles);
s = sin(angles);

val(1,1) = c(2)*c(3);
val(1,2) = s(1)*s(3) - c(1)*c(3)*s(2);
val(1,3) = c(1)*s(3)+s(1)*c(3)*s(2);

val(2,1) = s(2);
val(2,2) = c(1)*c(2);
val(2,3) = -s(1)*c(2);

val(3,1) = -c(2)*s(3);
val(3,2) = s(1)*c(3)+c(1)*s(2)*s(3);
val(3,3) = c(1)*c(3)-s(1)*s(2)*s(3);