function val = E(L,B)
% L - геодезическая долгота
% B - геодезическая широта

angles = [L B];
s = sin(angles);
c = cos(angles);

val  = zeros(3);

val(1,1) = -s(1);
val(1,2) = c(1);
val(1,3) = 0;

val(2,1) = -c(1)*s(2);
val(2,2) = -s(1)*s(2);
val(2,3) = c(2);

val(3,1) = c(1)*c(2);
val(3,2) = -c(2)*s(1);
val(3,3) = s(2);

end