function [r, phi] = cartesian_to_polar(points, sun, navigation)
%#codegen

x = points(:,1);
y = points(:,2);
sun_x = sun(1);
sun_y = sun(2);
biDistortion = navigation.biDistortion;
cx = biDistortion(4);
cy = biDistortion(3);

r = sqrt((x-sun_x).^2+(y-sun_y).^2);
phi = atan2((y-cy),(x-cx));
idx1 = phi<=-pi/2;
idx2 = phi>pi/2;
phi(idx1) = phi(idx1)+pi;
phi(idx2) = phi(idx2)-pi;