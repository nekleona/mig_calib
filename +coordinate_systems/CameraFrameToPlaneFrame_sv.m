function centers3D = CameraFrameToPlaneFrame(centers, data)%,  nTracksMax, nBlobsMax)
%#codegen
    OAR_Matrix = data.biLV_Matrix;
%     MGD_Matrix = data.biMGD_Matrix;
    MGD_Matrix = eye(3);
    
    biDistortion = data.biDistortion;
    
    cameraParams = struct('f',double(biDistortion([2 1])),...
        'c', double(biDistortion([4 3])),...
        'k', double(biDistortion(5:6)),...
        'alpha', 0);
    
    centers2D = calib.normalizePixel(centers', cameraParams);
    
    n = size(centers2D,2);
    x3D = [centers2D; ones(1,n)];
    
%     T2 = [0 0 1;...
%     0 -1 0;...
%     1 0 0]; % �����
    T2 = [0 -1 0;...
        0 0 1;...
        1 0 0];
    
    centers3D = MGD_Matrix'*OAR_Matrix'*T2'*x3D; %%%%

%     x = centers3D(1,:); % ��� oy ���������� ����� (������ � LV �������)
%     y = centers3D(2,:);
%     z = centers3D(3,:);
%     
%     r = sqrt(z.^2 + x.^2 + y.^2);
%     
%     beta = atan2(z,x);
%     alpha = asin(y./r);
%     
%     measurements = [beta; alpha]; 
%     
%     h = data.biVector1(3);
%     [centers3D, ind] = coordinate_systems.projectPointsOnSphere(centers3D, h);
%     measurements = measurements(:,ind);
    
%     ind = centers3D(2,:)<0;
%     centers3D = centers3D*h./repmat(abs(centers3D(2,:)),3,1);
%     centers3D = centers3D(:,ind);
%     measurements = measurements(:,ind);
    
    
    
end

