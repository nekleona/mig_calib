function measurements = pointsToAngles(centers3D)
%#codegen

    x = centers3D(1,:); % ��� oy ���������� ����� (������ � LV �������)
    y = centers3D(2,:);
    z = centers3D(3,:);
    
    r = sqrt(x.^2 + y.^2 + z.^2);
    
    beta = atan2(z,x);
    alpha = asin(y./r);
    
    measurements = [beta; alpha];
end

