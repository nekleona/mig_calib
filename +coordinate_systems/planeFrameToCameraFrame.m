function centers2D = planeFrameToCameraFrame(centers3D, data, fl, MGD_Matrix, T2)
%#codegen

if nargin < 3
    fl = 1;
end

    OAR_Matrix = data.biLV_Matrix; %%%
    
if nargin < 4
    MGD_Matrix = data.biMGD_Matrix;
end
%     MGD_Matrix = data.biMGD_Matrix;
    
    biDistortion = data.biDistortion;
    
if fl
    cameraParams = struct('f',double(biDistortion([1 2])),...
        'c', double(biDistortion([3 4])),...
        'k', double(biDistortion(5:6)),...
        'alpha', 0);
else
    cameraParams = struct('f',double(biDistortion([2 1])),...
    'c', double(biDistortion([4 3])),...
    'k', double(biDistortion(5:6)),...
    'alpha', 0);
end
    
    Exter = struct('w', zeros(3,1), 'T', zeros(3,1));
    
%     T2 = [0 0 1;...
%     0 -1 0;...
%     1 0 0]; % �����
if nargin < 5
    T2 = [0 -1 0;...
        0 0 1;...
        1 0 0];
end
    
    centers3D_matrix = T2*OAR_Matrix*MGD_Matrix*centers3D(1:3,:);
%     centers3D_matrix = OAR_Matrix*MGD_Matrix*centers3D(1:3,:);
    centers2D = calib.projectPoints(centers3D_matrix, Exter, cameraParams);
