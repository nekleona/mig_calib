function pointsOut = getLineCircleIntersection(center, radius, linePoint1, linePoint2, isDebug)

if nargin < 5
    isDebug = true;
%     center = [12 13 15];
%     radius = 5;
%     linePoint1 = [14 13 15];
%     linePoint2 = [1 1 1];

    center = rand(1,3)*10;
    radius = 10;
    linePoint1 = center;
    linePoint2 = center-15;
    
%     center = [0,0,0];
%     radius = 3100;
%     linePoint1 = 1.0e+04 *[-1.1827, -0.1668, -1.4127];
%     linePoint2 = [0, -1641, 0];
%     linePoint2 = rand(1,3)*10;
end
%     syms x1 y1 z1 x2 y2 z2 c1 c2 c3 r x y z p A B C D
%     
%     y = (x-x1)*(y1-y2)/(x1-x2)+y1;
%     z = (x-x1)*(z1-z2)/(x1-x2)+z1;
%     
%     p = (x - c1)^2 + (y - c2)^2 + (z - c3)^2 - r^2;
%     collect(p, x)
% % >> ((y1 - y2)^2/(x1 - x2)^2 + (z1 - z2)^2/(x1 - x2)^2 + 1)*x^2 +...
% % >> (- 2*c1 - (2*c2*(y1 - y2))/(x1 - x2) - (2*c3*(z1 - z2))/(x1 - x2))*x...
% % >> + c1^2 + c2^2 + c3^2 - r
    
    c1 = center(1);
    c2 = center(2);
    c3 = center(3);
    
    x1 = linePoint1(1);
    y1 = linePoint1(2);
    z1 = linePoint1(3);
    
    x2 = linePoint2(1);
    y2 = linePoint2(2);
    z2 = linePoint2(3);
    
    r = radius;
    
    A = ((y1 - y2)^2/(x1 - x2)^2 + (z1 - z2)^2/(x1 - x2)^2 + 1);
    B = (- 2*c1 - (2*(y1 - y2)*(c2 - y1 + (x1*(y1 - y2))/(x1 - x2)))/(x1 - x2) - (2*(z1 - z2)*(c3 - z1 + (x1*(z1 - z2))/(x1 - x2)))/(x1 - x2));%(- 2*c1 - (2*c2*(y1 - y2))/(x1 - x2) - (2*c3*(z1 - z2))/(x1 - x2));
    C = (c2 - y1 + (x1*(y1 - y2))/(x1 - x2))^2 + (c3 - z1 + (x1*(z1 - z2))/(x1 - x2))^2 + c1^2 - r^2;%c1^2 + c2^2 + c3^2 - R^2;
    D = B.^2 - 4.*A.*C;
    
%     ind = find(D>=0);

%     x_s = (-B+sqrt(D(ind)))./(2*A(ind)); %%%%%

if D>=0
    x_s = [(-B+sqrt(D))./(2*A) (-B-sqrt(D))./(2*A)]; %%%%%
    y_s = (x_s-x1)*(y1-y2)/(x1-x2)+y1;%x_s*(y1-y2)/(x1-x2);
    z_s = (x_s-x1)*(z1-z2)/(x1-x2)+z1;%x_s*(z1-z2)/(x1-x2);

%     pointsOut = zeros(size(points));
    pointsOut = [x_s; y_s; z_s];
else
    pointsOut = [];
end
    
if isDebug
    [xe,ye,ze] = sphere;
    xe = R*xe + c1;
    ye = R*ye + c2;
    ze = R*ze + c3;

    fig = figure('Units','Normalized','Position',[0.1 0.1 0.3 0.3]);
    axx = axes('Parent',fig);
%     mesh(xe,ye,ze,'FaceAlpha',0.5);
    for i = 1:size(pointsOut,2)
        hold on;
        plot3(pointsOut(1,i),pointsOut(2,i),pointsOut(3,i),'r*');
    end
    hold on;
    plot3(c1,c2,c3,'b*');
    hold on;
    plot3(x1,y1,z1,'b*');
    hold on;
    plot3(x2,y2,z2,'b*');
    hold on;
    line([x1,x2],[y1,y2],[z1,z2],'Parent',axx);
    axx.XLabel.String = 'z'; axx.YLabel.String = 'x'; axx.ZLabel.String = 'y';
    axx.View = [0, 90];
    axis equal;
end
    
end