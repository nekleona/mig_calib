function [ind, centers3D, measurements] = CameraFrameToPlaneFrame(centers, data)%,  nTracksMax, nBlobsMax)
%#codegen
    OAR_Matrix = data.biLV_Matrix;
    MGD_Matrix = data.biMGD_Matrix;
    
    biDistortion = data.biDistortion;
    
    cameraParams = struct('f',double(biDistortion([2 1])),...
        'c', double(biDistortion([4 3])),...
        'k', double(biDistortion(5:6)),...
        'alpha', 0);
    
%     x = centers(:,1);
%     y = centers(:,2);
%     if any((y <= 130 & y >= 120) & (x <= 106 & x >= 95))
%         ind_target = find((y <= 130 & y >= 120) & (x <= 106 & x >= 95));
%         ind_debug = find((y <= 130 & y >= 120) & (x <= 136 & x >= 95));
%     end
    
    centers2D = calib.normalizePixel(centers', cameraParams);
    
    n = size(centers2D,2);
    x3D = [centers2D; ones(1,n)];
    
%     T2 = [0 0 1;...
%     0 -1 0;...
%     1 0 0]; % �����
    T2 = [0 -1 0;...
        0 0 1;...
        1 0 0];
    
    centers3D = MGD_Matrix'*OAR_Matrix'*T2'*x3D; %%%%

    x = centers3D(1,:); % ��� oy ���������� ����� (������ � LV �������)
    y = centers3D(2,:);
    z = centers3D(3,:);
    
    r = sqrt(z.^2 + x.^2 + y.^2);
    
    beta = atan2(z,x);
    alpha = asin(y./r);
    
    measurements = [beta; alpha]; 
    ind = 0;
    
%     h = data.biVector1(3);
%     [centers3D, ind] = coordinate_systems.projectPointsOnSphere(centers3D, h);
%     measurements = measurements(:,ind);
    
%     ind = centers3D(2,:)<0;
%     centers3D = centers3D*h./repmat(abs(centers3D(2,:)),3,1);
%     centers3D = centers3D(:,ind);
%     measurements = measurements(:,ind);
    
    
    
end

