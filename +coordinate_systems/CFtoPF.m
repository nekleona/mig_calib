function centers3D = CFtoPF(centers, data)
%#codegen
    OAR_Matrix = data.biLV_Matrix;
    MGD_Matrix = data.biMGD_Matrix;
    
    biDistortion = data.biDistortion;
    
    cameraParams = struct('f',double(biDistortion([2 1])),...
        'c', double(biDistortion([4 3])),...
        'k', double(biDistortion(5:6)),...
        'alpha', 0);
    
    centers2D = calib.normalizePixel(centers', cameraParams);
    
    n = size(centers2D,2);
    x3D = [centers2D; ones(1,n)];
    
%     T2 = [0 0 1;...
%     0 -1 0;...
%     1 0 0]; % �����
    T2 = [0 -1 0;...
        0 0 1;...
        1 0 0];
    
    centers3D = MGD_Matrix'*OAR_Matrix'*T2'*x3D;
    
end