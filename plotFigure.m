function plotFigure

close all

% ������ 24.01.18, ��� 2 � 3 ���� ��������� �� 180 �������� (�� ��������
% ���� 3 �� ������ ���� ��������)
ypr = [ 0       pi/4        -pi;...        
        -pi/2   0           -pi/4+pi;...
        pi/2    0           3*pi/4+pi;...
        pi      pi/4        -pi;...
        0       -pi/6       -pi/2; ...
        -pi     -pi/6       pi/2;...
        0       0           0];

nCam = size(ypr,1);
dcm = cell(nCam,1);
for iCam = 1:nCam
    angles = ypr(iCam,:);
   dcm{iCam}  = navigation.angle2dcm_codegen(angles(1),angles(2),angles(3),'YZX');
end

figure;

C = 'k';

% P0{1} = [];
P_arr{1} = [1 0 0; 0 1 0; 0 0 1]';
T2_atris = [1 0 0; 0 0 -1; 0 1 0];

ax_name_camera = {'x, ��', 'y, ����', 'z, �����'};

for iCam = 1:numel(P_arr)
    
    ax = subplot(2,3,iCam);
    
    xlabel('atris_x');
    ylabel('atris_y');
    zlabel('atris_z');
    
    P = P_arr{iCam};
    px = P(:,1);
    py = P(:,2);
    pz = P(:,3);

%     g = 10;
    p = {px, py, pz};

    next_plot = get(ax, 'NextPlot');
    hold(ax,'on');

   
    R1 = dcm{iCam}; 

    for iAxis = 1:3

        p_plane = p{iAxis};
        p_im = T2_atris'*R1'*p_plane;
        
        p_tmp(1) = -p_im(2)/p_im(1);
        p_tmp(2) = p_im(3)/p_im(1);
        
%         p_tmp

        quiver3(-p_im(1),-p_im(2),-p_im(3),2*p_im(1),2*p_im(2),2*p_im(3),0,'Color','r','MaxHeadSize',0.3)
        p_im = p_im*1.1;
        text(p_im(1),p_im(2),p_im(3), sprintf(ax_name_camera{iAxis}),'Color','r')

    end
    
end


%%
if nargin==0
    grid on;
    axis equal
    hold(ax,'off');
%     view(135,20)
end

set(ax, 'NextPlot',next_plot);

end