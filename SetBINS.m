function p = SetBINS(setfX, setfY, setfZ)

% iCam = 2;
% [~, ~,LV_inv, LV_angles_inv] = get_LV;
% close all;
% 
% % ���� (��������), ������, ����
% % angles = round(rad2deg(LV_angles_inv));
% angles = LV_angles_inv;
% 
% setfX = angles(iCam,3);
% setfY = angles(iCam,1);
% setfZ = angles(iCam,2);

% disp(LV_inv{iCam});

%%

  %REAL setfX = binsZamer.kren;
  %REAL setfY = binsZamer.curs;
  %REAL setfZ = binsZamer.tangag;
  %REAL *p = g_nav.p;
  
  csx = cos(setfX);
  snx = sin(setfX);
  csy = cos(setfY);
  sny = sin(setfY);
  csz = cos(setfZ);
  snz = sin(setfZ);

  % ��� ������� ������������ �� ��� ����� [12:41:56 20.4.2010 BOBAH]
  % g_nav.tprBins = binsZamer.time_receiv;
  % - 500mks

  p(1) = csy*csz;
  p(2) = snx*sny - csx*csy*snz;
  p(3) = csx*sny + csy*snx*snz;
  p(4) = snz;
  p(5) = csx*csz;
  p(6) = -(csz*snx);
  p(7) = -(csz*sny);
  p(8) = csy*snx + csx*sny*snz;
  p(9) = csx*csy - snx*sny*snz;
  
  p = reshape(p, 3, 3);
  