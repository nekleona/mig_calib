function [LV_angles_opt, err] = autocalib(distort_0, LV_angles_0, T2, in_vect_2D, in_vect_3D)
%[p_out, LV_matrix, val] = test_calib
% ��������� ������������� ��������� ������, ����� ������, �������-��,
% ��������� �������������� �� ��������� �����������!!
% (������� �������� ��� ������ 1, ��� �������� �������� ������ ������ � 14
% �������� (������ ���� 89 ��������))
% ��� ����� 2-4 �� �������������
% ��������� ������� LV � ��������� (f,c,k) (������� alpha = 0)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin < 5
    % % ���������� ������ �� ����������� � ��������� ������� �������
    in_vect_2D = [281 236; 233 244; 124 109; 286 44; 67 175]';
    % ���������� ������ � ��������� �� � �� �� ������� �������
    in_vect_3D = [ -0.2482 0.8182 -0.5192;...
           0.0453 0.7782 -0.6264;...
           0.9530 0.3037 0.0007;...
           0.1270 0.8338 0.5374;...
           0.9130 -0.0582 -0.4044]';
end

if nargin < 3
    LV_angles_0 = [0 pi/4 pi]';
% ��������� ����������� ��� ��������� ���������� (���� ����� �� ���������
% ������)
    distort_0 =   [ ...
            158.9531,...
            159.7886,...
            126.2419,...
            151.3495,...
            0.0204,...
           -0.4127]';
% ������ ��� ������� ���� ������ ����� (��������� ����� � ������ �����
% ���������, ������ �������� � params.T2 (� init)
    T2 = [0 -1 0; 0 0 1; 1 0 0];
else
    if numel(LV_angles_0)==9
        [r1, r2, r3] = dcm2angle(LV_angles_0,'YZX');
        LV_angles_0 = [r1, r2, r3]';
    end
end
p0 = LV_angles_0;
p0 = double(p0);

% �����������
% lb = p0; lb(7:9) = -pi;
% ub = p0; ub(7:9) = pi;
lb = [];
ub = [];

% in_vect_3D = double(in_vect_3D);
% [N,M] = size(in_vect_3D);
% t_3D = 1:M;
% for iAx = 1:N
%     fit_3D = fit(t_3D', in_vect_3D(iAx,:)','fourier2');
%     in_vect_3D(iAx,:) = fit_3D(t_3D)';
% end
% 
% in_vect_2D = double(in_vect_2D);
% [N,M] = size(in_vect_2D);
% t_3D = 1:M;
% for iAx = 1:N
%     fit_3D = fit(t_3D', in_vect_2D(iAx,:)','fourier2');
%     in_vect_2D(iAx,:) = fit_3D(t_3D)';
% end

% �����������
options = optimoptions(@fmincon,'FunctionTolerance',1e-6,'OptimalityTolerance',1e-6,'Algorithm','interior-point');
[LV_angles_opt, err] = fmincon(@(p)f(p, in_vect_3D, in_vect_2D, T2, distort_0), p0, [], [],[],[],lb,ub,[],options);

end

% ������� out_vect = ||f(p,X3D) - x2D|| -> min
function out_vect = f(p, in_vect_3D, in_vect_2D,T2, distort)  
% size(in_vect_3D) = [3 N],
% size(in_vect_2D) = [2 N],
% size(out_vext) = [3 N],
% p = [f1 f2 c1 c2 k1 k2 a1 a2 a3];
LV = angle2dcm(p(1),p(2),p(3),'YZX');
cameraParams = struct('f',double(distort(1:2)),...
                      'c', double(distort(3:4)),...
                      'k', double(distort(5:6)),...
                      'alpha', 0);
centers2D = calib.normalizePixel(in_vect_2D, cameraParams);
n = size(centers2D,2);
x3D = [centers2D; ones(1,n)];
centers3D = LV'*T2'*x3D;

norm_3D =  sqrt(sum(in_vect_3D.^2));
in_vect_3D = bsxfun(@rdivide,in_vect_3D,norm_3D);
    
norm_3D =  sqrt(sum(centers3D.^2));
centers3D = bsxfun(@rdivide,centers3D,norm_3D);

N = size(in_vect_3D,2);
out_vect = zeros(1,N);
for i = 1:N
    a = centers3D(:,i);
    b = in_vect_3D(:,i);
    out_vect(i) = norm(a-b);
end
out_vect = norm(out_vect(:));
end