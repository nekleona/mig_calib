%Computes the normalized coordinates xn given the pixel coordinates x_kk
%and the intrinsic camera parameters fc, cc and kc.
%
%INPUT: 
%   x:      [2,N] array of feature coordinates on the image plane
%   Inter:  struct with intrinsic parameters f,c,alpha,k
%
%OUTPUT: 
%   xn:     [2,N] array of normalized feature coordinates on the image plane

function [x_normalized] = normalizePixel(x,Inter)

fc = Inter.f;
cc = Inter.c;
alpha_c = Inter.alpha;
kc = Inter.k;

% First: Subtract principal point, and divide by the focal length:
x_distort = [(x(1,:) - cc(1))/fc(1);(x(2,:) - cc(2))/fc(2)];

% Second: undo skew
x_distort(1,:) = x_distort(1,:) - alpha_c * x_distort(2,:);

if ~isempty(find(kc,1))
	% Third: Compensate for lens distortion:
	x_normalized = calib.compensateDistortion(x_distort,kc);
else
   x_normalized = x_distort;
end
