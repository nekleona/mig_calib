%x = comp_distortion_oulu(xd,k)
%
%Compensates for radial and tangential distortion. Model From Oulu university.
%For more informatino about the distortion model, check the forward projection mapping function:
%project_points.m
%
%INPUT: xd: distorted (normalized) point coordinates in the image plane (2xN matrix)
%       k: Distortion coefficients (radial and tangential) (4x1 vector)
%
%OUTPUT: x: undistorted (normalized) point coordinates in the image plane (2xN matrix)
%
%Method: Iterative method for compensation.
%
%NOTE: This compensation has to be done after the subtraction
%      of the principal point, and division by the focal length.

function x = compensateDistortion(xd,k)

x = xd; 				% initial guess

for kk = 1:20
    r = sqrt(sum(x.^2));
    phi = atan(r);
    k_radial = 1 + k(1) * phi + k(2) * phi.^2;
    x = xd ./ (ones(2,1)*k_radial);
end
