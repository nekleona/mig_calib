function plotImages(ypr, T2, p_plane)

close all


g = 100;
px = g*[1 0 0]';
py = g*[0 1 0]';
pz = g*[0 0 1]';
p = {px, py, pz};
ax_name_plane = {'x, ������', 'y, ����', 'z, �����'};
ax_name_camera = {'x, ��', 'y, ����', 'z, �����'};

% ������ 24.01.18, ��� 2 � 3 ���� ��������� �� 180 �������� (�� ��������
% ���� 3 �� ������ ���� ��������)
ypr = [ 0       pi/4        -pi;...        
        -pi/2   0           -pi/4+pi;...
        pi/2    0           3*pi/4+pi;...
        pi      pi/4        -pi;...
        0       -pi/6       -pi/2; ...
        -pi     -pi/6       pi/2;...
        0       0           0];
    
T2 = [1 0 0; 0 0 -1; 0 1 0];

I = imread('���_����������e0.dat0x44_screenshot.bmp');
I = rgb2gray(I);
I = im2double(I);
I = imresize(I, [200, 200]);
% I = imrotate(I,90);
I = fliplr(I);
I = flipud(I);

colors = I(:);
colors = repmat(colors, 1, 3);

% imwrite(I, 'test.png');
% return

[n,m] = size(I);

% cam 1
x = 100*ones(1,n*m);
[y, z] = meshgrid(n:-1:1, 1:m);
y = y(:)' - m/2;
z = z(:)' - n/2;
p_plane{1} = [x; y; z];

ax = axes;
xlabel(ax,'x');
ylabel(ax,'y');
zlabel(ax,'z');
grid on;
view(121,30)

for iAxis = 1:3
    
    hold on;
    
    angles = ypr(1,:);
    R1  = navigation.angle2dcm_codegen(angles(1),angles(2),angles(3),'YZX');

    p_plane_2 = p{iAxis};
    quiver3(-p_plane_2(1),-p_plane_2(2),-p_plane_2(3),2*p_plane_2(1),2*p_plane_2(2),2*p_plane_2(3),0,'Color','b','MaxHeadSize',0.2)
    p_plane_2 = p_plane_2*1.1;
    text(p_plane_2(1),p_plane_2(2),p_plane_2(3), sprintf(ax_name_plane{iAxis}),'Color','b')

    p_cam = R1'*p_plane_2;

    p_cam = 0.5*p_cam;
    quiver3(-p_cam(1),-p_cam(2),-p_cam(3),2*p_cam(1),2*p_cam(2),2*p_cam(3),0,'Color','r','MaxHeadSize',0.3)
    p_cam = p_cam*1.1;
    text(p_cam(1),p_cam(2),p_cam(3), sprintf(ax_name_camera{iAxis}),'Color','r')

end

hold on;
scatter3(x, y, z, 5, colors);

%%
figure;

% cam 1
x = 1e6*ones(1,n*m);
[y, z] = meshgrid(n:-1:1, 1:m);
y = y(:)' - m/2;
z = z(:)' - n/2;
p_plane{1} = [x; y; z];

for iCam = 1
    
   angles = ypr(iCam,:);
   R1  = navigation.angle2dcm_codegen(angles(1),angles(2),angles(3),'YZX');
     
   p_tmp = T2'*R1'*p_plane{iCam};
     
   p_im = zeros(2, size(p_tmp,2));
   
   p_im(1,:) = -p_tmp(2,:)./p_tmp(1,:);
   p_im(2,:) = p_tmp(3,:)./p_tmp(1,:);
   
   ax = subplot(2,3,iCam);
   ax.YDir = 'reverse';
    
   hold on;
   scatter(p_im(1,:), p_im(2,:), 5, colors);
   
   xlabel('x');
   ylabel('y');
   
end


end