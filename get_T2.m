function get_T2

close all;

I = imread('���_����������e0.dat0x44_screenshot.bmp');
I = rgb2gray(I);
I = im2double(I);
I = imresize(I, [200, 150]);
I = I';
I_lr = fliplr(I);

[n_row, n_col] = size(I);

z_im = ones(1,n_row*n_col);
[x_im, y_im] = meshgrid(1:n_row, 1:n_col);
x_im = x_im(:)';
y_im = y_im(:)';

[inv_x_im, inv_y_im] = meshgrid(n_row:-1:1, n_col:-1:1);
inv_x_im = inv_x_im(:)';
inv_y_im = inv_y_im(:)';

%%

iCam = 1;

points{iCam} = [z_im; inv_x_im; y_im];

x = points{iCam}(1,:);
y = points{iCam}(2,:);
z = points{iCam}(3,:);
linearInd = sub2ind([n_row, n_col], y, z);
colors{iCam} = I_lr(linearInd);
colors{iCam} = colors{iCam}(:);
colors{iCam} = repmat(colors{iCam}, 1, 3);

x = 100*x;
points{iCam} = [x; inv_x_im - n_row/2; y_im - n_col/2];

% iCam = 4;

% points{iCam} = [z_im; inv_x_im; y_im];

% x = points{iCam}(1,:);
% y = points{iCam}(2,:);
% z = points{iCam}(3,:);
% linearInd = sub2ind([n_row, n_col], y, z);
% colors{iCam} = I(linearInd);
% colors{iCam} = colors{iCam}(:);
% colors{iCam} = repmat(colors{iCam}, 1, 3);
% 
% x = -10000*x;
% points{iCam} = [x; inv_x_im - n_row/2; y_im - n_col/2];

%%

ypr = [ 0      0        0;...        
        0       0           0];

% ypr = [ pi      pi/4        -pi;...        
%         0       0           0];
    
T2_atris = [1 0 0;
    0 0 1;
    0 -1 0];

nCam = size(ypr,1);
dcm = cell(nCam,1);
for iCam = 1:nCam
    angles = ypr(iCam,:);
   dcm{iCam}  = navigation.angle2dcm_codegen(angles(1),angles(2),angles(3),'YZX');
end
nCam = size(ypr,1)-numel(find(sum(ypr,2)==0));

figure('Units','normalized','Position',[0.1 0.1 0.7 0.7]);

g = 100;
p0 = [0 0 0]';
px = g*[1 0 0]';
py = g*[0 1 0]';
pz = g*[0 0 1]';
p = {px, py, pz};
ax_name_plane = {'x, ������', 'y, ����', 'z, �����'};
% ax_name_camera = {'x, ��', 'y, �����', 'z, ���'};
ax_name_camera = {'x, ��', 'y, ����', 'z, �����'};

%%

iCam = 1;

% sbp = subplot(2,3,iCam);
% sbp = axes;
sbp = subplot(1,2,1);
hold(sbp,'on');

xlabel(sbp,'x');
ylabel(sbp,'y');
zlabel(sbp,'z');
grid on;
% view(121,30)
view(217,-44)

R1 = dcm{iCam}; 
R2 = dcm{end};

% ������� �������� �� ��������� �� � �� ������ � MATLAB
T = [0 -1 0; 0 0 -1; 1 0 0];

% plotCamera('Location', p0, 'Orientation', T*R2, 'Opacity',0,'Color','b');
% plotCamera('Location', p0, 'Orientation', T*R1, 'Opacity',0);

for iAxis = 1:3

    p_plane = p{iAxis};
    quiver3(-p_plane(1),-p_plane(2),-p_plane(3),2*p_plane(1),2*p_plane(2),2*p_plane(3),0,'Color','b','MaxHeadSize',0.2)
    p_plane = p_plane*1.1;
    text(p_plane(1),p_plane(2),p_plane(3), sprintf(ax_name_plane{iAxis}),'Color','b')

    p_cam = R1'*p_plane;

    p_cam = 0.5*p_cam;
    quiver3(-p_cam(1),-p_cam(2),-p_cam(3),2*p_cam(1),2*p_cam(2),2*p_cam(3),0,'Color','r','MaxHeadSize',0.3)
    p_cam = p_cam*1.1;
    text(p_cam(1),p_cam(2),p_cam(3), sprintf(ax_name_camera{iAxis}),'Color','r')

end

hold on;
x = points{iCam}(1,:);
y = points{iCam}(2,:);
z = points{iCam}(3,:);
scatter3(x, y, z, 5, colors{iCam});

%%

% figure;
iCam = 1;

R1 = dcm{iCam};
p_tmp = T2_atris'*R1*points{iCam};
p_im = zeros(2, size(p_tmp,2));
p_im(1,:) = -p_tmp(2,:)./p_tmp(1,:);
p_im(2,:) = p_tmp(3,:)./p_tmp(1,:);


%ax = subplot(2,3,iCam);
% ax = axes;
ax = subplot(1,2,2);
ax.YDir = 'reverse';

hold on;
scatter(p_im(1,:), p_im(2,:), 100, colors{iCam});

set(gca,'xtick',[])
set(gca,'ytick',[])

ax.XAxisLocation = 'top';

title(sprintf('cam %u', iCam));

axis equal;

xlabel('x');
ylabel('y');

end