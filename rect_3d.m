function rect_3d(p0,sizes,ax,R,one_color)

next_plot = get(ax, 'NextPlot');
hold(ax,'on');

colors.bottom       = one_color;
colors.top          = one_color;
colors.forward      = one_color;
colors.back         = one_color;
colors.left         = one_color;
colors.right        = one_color;
colors.sphere       = one_color;
colors.tube         = one_color;

colors.FaceAlpha = 1;

x0 = p0(1);
y0 = p0(2);
z0 = p0(3);

w = sizes(1);
d = sizes(2);
h = sizes(3);

%% ������ ���������

points = [
    d/2     -w/2    -h/2;
    -d/2    -w/2    -h/2;
    -d/2    w/2     -h/2;
    d/2     w/2     -h/2;
    ];

% points = R*points';
% points = points';

points(:,1) = points(:,1) + x0;
points(:,2) = points(:,2) + y0;
points(:,3) = points(:,3) + z0;

points = R*points';
points = points';

patch('XData',points(:,1),'YData',points(:,2),'ZData',points(:,3),'FaceColor',colors.bottom,...
    'FaceAlpha',colors.FaceAlpha,'Parent',ax);

%% ������� ���������

points = [
    d/2     -w/2    h/2;
    -d/2    -w/2    h/2;
    -d/2    w/2     h/2;
    d/2     w/2     h/2;
    ];

% points = R*points';
% points = points';

points(:,1) = points(:,1) + x0;
points(:,2) = points(:,2) + y0;
points(:,3) = points(:,3) + z0;

points = R*points';
points = points';

patch('XData',points(:,1),'YData',points(:,2),'ZData',points(:,3),'FaceColor',colors.top,...
    'FaceAlpha',colors.FaceAlpha,'Parent',ax);

%% �������� ���������

points = [
    d/2     -w/2    -h/2;
    d/2     -w/2    h/2;
    d/2     w/2     h/2;
    d/2     w/2     -h/2;
    ];

% points = R*points';
% points = points';

points(:,1) = points(:,1) + x0;
points(:,2) = points(:,2) + y0;
points(:,3) = points(:,3) + z0;

points = R*points';
points = points';

patch('XData',points(:,1),'YData',points(:,2),'ZData',points(:,3),'FaceColor',colors.forward,...
    'FaceAlpha',colors.FaceAlpha,'Parent',ax);

%% ������ ���������

points = [
    -d/2     -w/2    -h/2;
    -d/2     -w/2    h/2;
    -d/2     w/2     h/2;
    -d/2     w/2     -h/2;
    ];

% points = R*points';
% points = points';

points(:,1) = points(:,1) + x0;
points(:,2) = points(:,2) + y0;
points(:,3) = points(:,3) + z0;

points = R*points';
points = points';

patch('XData',points(:,1),'YData',points(:,2),'ZData',points(:,3),'FaceColor',colors.back,...
    'FaceAlpha',colors.FaceAlpha,'Parent',ax);

%% ����� ���������

points = [
    d/2     -w/2    -h/2;
    d/2     -w/2    h/2;
    -d/2     -w/2     h/2;
    -d/2     -w/2     -h/2;
    ];

% points = R*points';
% points = points';

points(:,1) = points(:,1) + x0;
points(:,2) = points(:,2) + y0;
points(:,3) = points(:,3) + z0;

points = R*points';
points = points';

patch('XData',points(:,1),'YData',points(:,2),'ZData',points(:,3),'FaceColor',colors.left,...
    'FaceAlpha',colors.FaceAlpha,'Parent',ax);

%% ������ ���������

points = [
    d/2     w/2    -h/2;
    d/2     w/2    h/2;
    -d/2     w/2     h/2;
    -d/2     w/2     -h/2;
    ];

% points = R*points';
% points = points';

points(:,1) = points(:,1) + x0;
points(:,2) = points(:,2) + y0;
points(:,3) = points(:,3) + z0;

points = R*points';
points = points';

patch('XData',points(:,1),'YData',points(:,2),'ZData',points(:,3),'FaceColor',colors.right,...
    'FaceAlpha',colors.FaceAlpha,'Parent',ax);

%%

set(ax, 'NextPlot',next_plot);

% hold(ax,'off');