function horiz

load('navigation.mat');
load('navigation_cam3.mat');
load('screenshot_cam3.mat');

[LV_Matrix, ~, ~, ~] = get_LV;
close all;

% LV_Matrix = matfile('LV_matrix.mat');
% LV_Matrix = LV_Matrix.LV;
% close all;

figure('Units','normalized','Position',[0.1 0.1 0.7 0.7]);

for iCam = 1:6
    
    filepath = 'D:\Repos\mig_calib\plane_images';
    filename = sprintf('���_����������e0.dat0x4%u_plane.bmp',iCam);
    
    if iCam==3
        im = I3';
    else
        im = imread(fullfile(filepath,filename));
    end
    
    sbp = subplot(2,3,iCam);
    hold(sbp,'on');
    grid(sbp,'on');

%     im = permute(im,[2 1 3]);
%     im = fliplr(im);
    imshow(im);
    
    [x,y] = meshgrid(1:40:size(im,2),1:40:size(im,1));
    centers2D = [x(:), y(:)];
    
    %%%%%
    biDistortion = [2*163.1631, 2*156.3190, 2*123.5296, 2*157.5202,  0.0233, -0.4132];
    
    cameraParams = struct('f',double(biDistortion([2 1])),...
    'c', double(biDistortion([4 3])),...
    'k', double(biDistortion(5:6)),...
    'alpha', 0);

    centers2D_tmp = calib.normalizePixel(centers2D', cameraParams);
	centers2D_tmp = centers2D_tmp';
    centers3D = [ones(size(centers2D_tmp,1),1), -centers2D_tmp(:,1), centers2D_tmp(:,2)]';
   
    if iCam==3
        NPSK = (nav3.biMGD_Matrix.Data)';
    else
        NPSK = (nav{iCam}.biMGD_Matrix.Data)';
    end
    
    T2_atris = [1 0 0;
            0 0 1;
            0 -1 0];
       
    LV = LV_Matrix{iCam};
    
    centers3D = NPSK*LV*T2_atris*centers3D;
    
    if iCam==3
        h_plane = nav3.biVector1.Data(3);
    else
        h_plane = nav{iCam}.biVector1.Data(3);
    end
    
    
    [~, idx_ground] = coordinate_systems.projectPointsOnSphere(centers3D, h_plane);
    idx_air = ~idx_ground;
    
    plot(centers2D(idx_air,1),centers2D(idx_air,2),'b+','Parent',sbp);
    plot(centers2D(~idx_air,1),centers2D(~idx_air,2),'r+','Parent',sbp);
    
    hold(sbp,'off');
   
end

end