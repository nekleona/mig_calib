fileID = fopen('SU_selex_mask\new2_MASK_Selex_obj_C5_320x256_il.bin');
mask = zeros(320, 256, 6);


for i = 1:6  
    mask(:,:,i) = fread(fileID,[320,256],'ubit1');  
end

% Отображение маски i
i = 4;
M = mask(:,:,i);

fig = figure('Units', 'Pixels');
pos = get(fig, 'Position');
pos(3:4) = 1.5*size(M);
set(fig, 'Position', pos);
ax = axes('Units','Normalized','Position',[0.05 0 0.95 0.95]);
ax.XAxisLocation = 'top';
% ax.xLabel.string = 'x';
% ax.yLabel.string = 'y';
xlabel('x, w');
ylabel('y, h');
set(ax, 'xtick', []);
set(ax, 'ytick', []);
grid on;
axes('Units','Normalized','Position',[0.05 0 0.95 0.95]);
M_new = M';
M_new(1,:) = 0;
M_new(end,:) = 0;
M_new(:,1) = 0;
M_new(:,end) = 0;
imshow(M_new)