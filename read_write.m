function read_write

% w = 320;
% h = 256;
% 
% %%
% 
% % fileID = fopen('SU-6.kzz');
% % fileID = fopen('MIG.kzz');
% % 
% % for iCam = 1:6
% %     BW = fread(fileID,[h w],'ubit1');
% %     BW = reshape(BW,h,w);
% %     imshow(BW);
% % end
% % 
% % fclose(fileID);
% 
% %%
% 
% % w = 320;
% % h = 256;
% % 
% % fileID = fopen('MIG.kzz','w');
% % 
% % for i = 1:6
% %     BW = false(h,w);
% %     fwrite(fileID,BW,'ubit1');
% % end
% % 
% % fclose(fileID);
% 
% %%
% 
% fileID = fopen('MIG.kzz','w');
% 
% figure;
% for i = 1:6
%     
%     mask_name = fullfile('mask_mig', ['mask', num2str(i),'.bin']);
%     fileID_cur = fopen(mask_name);
%     BW = fread(fileID_cur,[640,512],'ubit1');
% %     imshow(BW);
%     fclose(fileID_cur);
%     fwrite(fileID,BW','ubit1');
%     
%     subplot(2,3,i);
%     imshow(BW);
% end
% 
% fclose(fileID);
% 
% fclose('all');
% 
% %%
% fileID = fopen('SU_selex_mask\new2_MASK_Selex_obj_C5_320x256_il.bin');
% mask = zeros(320, 256, 6);
% 
% 
% figure;
% for i = 1:6  
%     
%     mask(:,:,i) = fread(fileID,[320,256],'ubit1');  
%     subplot(2,3,i);
%     imshow(mask(:,:,i));
% end
% 
% fclose(fileID);
% 
% fileID = fopen('SU.kzz','w');
% for i = 1:6
%     BW = mask(:,:,i);
%     fwrite(fileID,BW','ubit1');
% end
% fclose(fileID);
% 
% fclose('all');
%%

%%
% clc;

% LV1 =     [0.0000         0   -1.0000;...
%     0.7071    0.7071    0.0000;...
%     0.7071   -0.7071    0.0000]

% fileID = fopen('SU_Adjust.mxp');

% fileID = fopen('MIG_Adjust.mxp');
% fileID = fopen('Atris_sn_00000001_Adjust.bin');


for i = 1:6
    iLV = fread(fileID,9,'single');
    iLV = reshape(iLV,3,3);
    disp(iLV);
end

fclose(fileID);

%%

% clc;
% 
% [~, ~, LV_inv, ~] = get_LV;
% close all;
% 
% 
% load('LV_matrix.mat');
% 
% % fileID = fopen('MIG_Adjust.mxp','w');
% 
% 
% 
% for iCam = 1:6
%     
%     tmp = LV_inv{iCam};
% 
%     for i = 1:numel(tmp)
%         if abs(tmp(i) - 1)<1e-8
%             tmp(i) = 1;
%         end
%         if abs(tmp(i) + 1)<1e-8
%             tmp(i) = -1;
%         end
%         if abs(tmp(i) - 0.5)<1e-8
%             tmp(i) = 0.5;
%         end
%         if abs(tmp(i) + 0.5)<1e-8
%             tmp(i) = -0.5;
%         end
%         if abs(tmp(i))<1e-8
%             tmp(i) = 0;
%         end
%     end
% 
% %     fwrite(fileID,tmp','single');
% tmp
% end

% fclose(fileID);

%%

% clc;
% T2 = [0 1 0; 0 0 1; 1 0 0];
% 
% load('LV_matrix.mat');
% 
% fileID = fopen('MIG_Adjust_2.mxp','w');
% 
% fwrite(fileID,T2,'single');
% 
% fclose(fileID);

%%

fileID = fopen('Adjustment_Selex.bin');
% fileID = fopen('Adjustment_MIG.bin');

angles = [];
for iCam = 1:6
    tmp = fread(fileID,[1 3],'single');
    angles = [angles; tmp];
end
disp(angles)
fclose(fileID);

%%

filename = sprintf('Selex_Adjust.bin');
fileID = fopen(filename,'w');
m_aircraft_camera = cell(6,1);
for iCam = 1:6
    tmp = angles(iCam,:);
    tmp = tmp([1 3 2]);
    tmp = deg2rad(tmp);
    LV_inv = navigation.angle2dcm_codegen(tmp(1),tmp(2),tmp(3),'YZX');
    m_aircraft_camera{iCam} = LV_inv';
    fwrite(fileID, LV_inv, 'single');
end
fclose(fileID);
save('m_aircraft_camera.mat', 'm_aircraft_camera');

%%

filename = sprintf('Selex_Adjust_old.bin');
fileID = fopen(filename);
for iCam = 1:6
   LV = fread(fileID, [3,3], 'single');
%     if iCam == 3
        LV
%     end
end
fclose(fileID);

%%

filename = sprintf('Selex_Adjust.bin');
fileID = fopen(filename);
for iCam = 1:6
    LV = fread(fileID, [3,3], 'single');
%     if iCam == 3
        LV
%     end
end
fclose(fileID);

%%

[~, ~,~, LV_angles_inv] = get_LV;
close all;

angles = round(rad2deg(LV_angles_inv));
% ��������, ������, ���� -> ��������, ����, ������
angles = angles(:,[1 3 2])';

fileID = fopen('Adjustment_MIG.bin','w');
fwrite(fileID,angles(:),'single');
fclose(fileID);

%%

% fileID = fopen('Adjustment_MIG_main.bin');
fileID = fopen('Adjustment_Selex.bin');
angles = fread(fileID, 3*6, 'single');
angles = reshape(angles, 3, 6);
angles = angles';
angles = angles(:,[1 3 2]);
fclose(fileID);

%%
% [~, ~, LV_inv, ~] = get_LV;
% close all;
% 
% fileID = fopen('Adjustment_MIG.bin','w');
% fwrite(fileID,LV_inv','single');
% fclose(fileID);
%%
% 
% load('distortion.mat');
% fileID = fopen('Distortion_MIG_320_256.bin','w');
% fwrite(fileID,distortion(:),'single');
% fclose(fileID);
% fileID = fopen('Distortion_MIG_640_512.bin','w');
% fwrite(fileID,[2 2 2 2 1 1]'.*distortion(:),'single');
% fclose(fileID);

%%
% % fileID = fopen('Distortion_MIG_320_256.bin');
% fileID = fopen('Distortion_MIG_640_512.bin');
% distortion = [];
% for iElement = 1:6
%     tmp = fread(fileID,1,'single');
%     distortion = [distortion; tmp];
% end
% disp(distortion)
% fclose(fileID);
%%
fileID = fopen('Atris_Matrix.bin');
T2_inv = fread(fileID, [3,3], 'single')
fclose(fileID);