function [LV, LV_angles,LV_inv, LV_angles_inv, T2_atris] = get_LV

close all;

I = imread('���_����������e0.dat0x44_screenshot.bmp');
I = rgb2gray(I);
I = im2double(I);
I = imresize(I, [200, 150]);
I = I';
I_lr = fliplr(I);
% I = flipud(I);
% I = I';

% imshow(I);

[n_row, n_col] = size(I);

z_im = ones(1,n_row*n_col);
[x_im, y_im] = meshgrid(1:n_row, 1:n_col);
x_im = x_im(:)';
y_im = y_im(:)';

[inv_x_im, inv_y_im] = meshgrid(n_row:-1:1, n_col:-1:1);
inv_x_im = inv_x_im(:)';
inv_y_im = inv_y_im(:)';

%%

iCam = 1;

points{iCam} = [z_im; inv_x_im; y_im];

x = points{iCam}(1,:);
y = points{iCam}(2,:);
z = points{iCam}(3,:);
linearInd = sub2ind([n_row, n_col], y, z);
colors{iCam} = I_lr(linearInd);
colors{iCam} = colors{iCam}(:);
colors{iCam} = repmat(colors{iCam}, 1, 3);

x = 10000*x;
points{iCam} = [x; inv_x_im - n_row/2; y_im - n_col/2];

%%

iCam = 2;

points{iCam} = [inv_y_im; inv_x_im; z_im];

x = points{iCam}(1,:);
y = points{iCam}(2,:);
z = points{iCam}(3,:);
linearInd = sub2ind([n_row, n_col], y, x);
colors{iCam} = I(linearInd);
colors{iCam} = colors{iCam}(:);
colors{iCam} = repmat(colors{iCam}, 1, 3);

z = 10*z;
points{iCam} = [inv_y_im - n_col/2; inv_x_im - n_row/2; z];

%%

iCam = 3;

points{iCam} = [y_im; inv_x_im; z_im];

x = points{iCam}(1,:);
y = points{iCam}(2,:);
z = points{iCam}(3,:);
linearInd = sub2ind([n_row, n_col], y, x);
colors{iCam} = I_lr(linearInd);
colors{iCam} = colors{iCam}(:);
colors{iCam} = repmat(colors{iCam}, 1, 3);

z = -10*z;
points{iCam} = [y_im - n_col/2; inv_x_im - n_row/2; z];

%%

iCam = 4;

points{iCam} = [z_im; inv_x_im; y_im];

x = points{iCam}(1,:);
y = points{iCam}(2,:);
z = points{iCam}(3,:);
linearInd = sub2ind([n_row, n_col], y, z);
colors{iCam} = I(linearInd);
colors{iCam} = colors{iCam}(:);
colors{iCam} = repmat(colors{iCam}, 1, 3);

x = -10000*x;
points{iCam} = [x; inv_x_im - n_row/2; y_im - n_col/2];

%%

iCam = 5;

points{iCam} = [z_im; inv_x_im; y_im];

x = points{iCam}(1,:);
y = points{iCam}(2,:);
z = points{iCam}(3,:);
linearInd = sub2ind([n_row, n_col], y, z);
colors{iCam} = I_lr(linearInd);
colors{iCam} = colors{iCam}(:);
colors{iCam} = repmat(colors{iCam}, 1, 3);

x = 10000*x;
points{iCam} = [x; inv_x_im - n_row/2; y_im - n_col/2];

%%

iCam = 6;

points{iCam} = [z_im; inv_x_im; y_im];

x = points{iCam}(1,:);
y = points{iCam}(2,:);
z = points{iCam}(3,:);
linearInd = sub2ind([n_row, n_col], y, z);
colors{iCam} = I(linearInd);
colors{iCam} = colors{iCam}(:);
colors{iCam} = repmat(colors{iCam}, 1, 3);

x = -10000*x;
points{iCam} = [x; inv_x_im - n_row/2; y_im - n_col/2];


%%

% ������ ��� �� ������ �����

% % ������ 23.06.17, ��� 2 � 3 ���� �������� �������, ��� �� �� 
% % --- �������� �� �����
% ypr = [ 0       pi/4        -pi;...        
%         -pi/2   0           -pi/4;...
%         pi/2    0           3*pi/4;...
%         pi      pi/4        -pi;...
%         0       -pi/6       -pi/2; ...
%         -pi     -pi/6       pi/2;...
%         0       0           0];

% ������ 24.01.18, ��� 2 � 3 ���� ��������� �� 180 �������� (�� ��������
% ���� 3 �� ������ ���� ��������)
ypr = [ 0       pi/4        -pi;...        
        -pi/2   0           -pi/4+pi;...
        pi/2    0           3*pi/4+pi-2*pi;...
        pi      pi/4        -pi;...
        0       -pi/6       -pi/2; ...
        -pi     -pi/6       pi/2;...
        0       0           0];
    
% T2_atris = [1 0 0; 0 0 -1; 0 1 0];

T2_atris = [1 0 0;
        0 0 1;
        0 -1 0];


nCam = size(ypr,1);
dcm = cell(nCam,1);
for iCam = 1:nCam
    angles = ypr(iCam,:);
   dcm{iCam}  = navigation.angle2dcm_codegen(angles(1),angles(2),angles(3),'YZX');
end
nCam = size(ypr,1)-numel(find(sum(ypr,2)==0));

%%

figure('Units','normalized','Position',[0.1 0.1 0.7 0.7]);

g = 100;
p0 = [0 0 0]';
px = g*[1 0 0]';
py = g*[0 1 0]';
pz = g*[0 0 1]';
p = {px, py, pz};
ax_name_plane = {'x, ������', 'y, ����', 'z, �����'};
% ax_name_camera = {'x, ��', 'y, �����', 'z, ���'};
ax_name_camera = {'x, ��', 'y, ����', 'z, �����'};

for iCam = 1:6%1:nCam
    sbp = subplot(2,3,iCam);
    hold(sbp,'on');
    
    xlabel(sbp,'x');
    ylabel(sbp,'y');
    zlabel(sbp,'z');
%     xlim(sbp,[-10 10]);
%     ylim(sbp,[-10 10]);
%     zlim(sbp,[-10 10]);
    grid on;
    view(121,30)
%     view(90,0)

    R1 = dcm{iCam}; 
    R2 = dcm{end};
    
    % ������� �������� �� ��������� �� � �� ������ � MATLAB
    T = [0 -1 0; 0 0 -1; 1 0 0];
    
    plotCamera('Location', p0, 'Orientation', T*R2, 'Opacity',0,'Color','b');
    plotCamera('Location', p0, 'Orientation', T*R1, 'Opacity',0);
   
   
    for iAxis = 1:3
        
        p_plane = p{iAxis};
        quiver3(-p_plane(1),-p_plane(2),-p_plane(3),2*p_plane(1),2*p_plane(2),2*p_plane(3),0,'Color','b','MaxHeadSize',0.2)
        p_plane = p_plane*1.1;
        text(p_plane(1),p_plane(2),p_plane(3), sprintf(ax_name_plane{iAxis}),'Color','b')

        p_cam = R1'*p_plane;
        
        p_cam = 0.5*p_cam;
        quiver3(-p_cam(1),-p_cam(2),-p_cam(3),2*p_cam(1),2*p_cam(2),2*p_cam(3),0,'Color','r','MaxHeadSize',0.3)
        p_cam = p_cam*1.1;
        text(p_cam(1),p_cam(2),p_cam(3), sprintf(ax_name_camera{iAxis}),'Color','r')
                
    end
    
    hold on;
    x = points{iCam}(1,:);
    y = points{iCam}(2,:);
    z = points{iCam}(3,:);
    scatter3(x, y, z, 5, colors{iCam});
   
end

% close all;

%%
figure;
for iCam = 1:6
    R1 = dcm{iCam};
    p_tmp = T2_atris'*R1*points{iCam};
    p_im = zeros(2, size(p_tmp,2));
    p_im(1,:) = -p_tmp(2,:)./p_tmp(1,:);
    p_im(2,:) = p_tmp(3,:)./p_tmp(1,:);

   
    ax = subplot(2,3,iCam);
    ax.YDir = 'reverse';

    hold on;
    scatter(p_im(1,:), p_im(2,:), 10, colors{iCam});
    
    set(gca,'xtick',[])
    set(gca,'ytick',[])
    
    ax.XAxisLocation = 'top';
    
    title(sprintf('cam %u', iCam));

    xlabel('x');
    ylabel('y');
end

%%

LV = cell(nCam,1);
LV_inv = cell(nCam,1);
LV_angles = zeros(nCam,3);
LV_angles_inv = zeros(nCam,3);
for iCam = 1:nCam
    LV_inv{iCam} = dcm{iCam};
    LV{iCam} = LV_inv{iCam}';
    [r1, r2, r3]  = dcm2angle(LV{iCam},'YZX');
    LV_angles(iCam,:) = [r1, r2, r3];
    [r1, r2, r3]  = dcm2angle(LV{iCam}','YZX');
    LV_angles_inv(iCam,:) = [r1, r2, r3];
end
% rad2deg(LV_angles)
rad2deg(LV_angles_inv)
save('LV_matrix.mat','LV','LV_angles','LV_inv','LV_angles_inv');
end

