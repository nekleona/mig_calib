function cam2aircraft()


biDistortion = [170.1198 163.8111 160 128 0.0261 -0.4071];

cameraParams = struct('f',double(biDistortion([1 2])),...
'c', double(biDistortion([3 4])),...
'k', double(biDistortion(5:6)),...
'alpha', 0);

T2_atris = [1 0 0;
            0 0 1;
            0 -1 0];

%%

filepath = 'D:\Repos\mig_calib\plane_images';
% filename = 'ses2file0minute0-8.dat0x42_23783.bmp';
% filename = 'ses1file0minute2-3-013-oar2.dat0x42_0.bmp';
% filename = 'oar2_detector.png';
filename = 'oar4_detector.png';
im = imread(fullfile(filepath,filename));
    
%%

[LV_Matrix, ~, ~, ~] = get_LV;
close all;

m = matfile('mig_lv_test.mat');
T2_inv_LV_inv = m.LV_inv;

% iCam = 2;
% oar = evalin('base','oar2');
iCam = 4;
oar = evalin('base','oar4');
LV = LV_Matrix{iCam};


%%
figure('Units','Pixels','Position',[200 200 320 256]);
ax = axes('Units','Normalize','Position',[0 0 1 1]);
imshow(im);


%%

[X, Y] = meshgrid(1:320, 1:256);
X = X';
Y = Y';
centers2D = [X(:), Y(:)];

[~, az_angle, el_angle] = c2a(centers2D, cameraParams, T2_atris, LV);
% angle_matrix = reshape(az_angle,size(X,1),size(X,2));
angle_matrix = reshape(el_angle,size(X,1),size(X,2));

hold on;
contour(X,Y,angle_matrix,'ShowText','on','LabelSpacing',144);


%%


% centers2D = [0, 256; 160, 128; 320, 0];
% centers2D = [1, 1; 160, 128; 233, 188; 320, 256];
% centers2D = [linspace(0,320,10)', linspace(0,256,10)'];

% centers2D = oar.points;
% 
% [~, az_angle, el_angle] = c2a(centers2D, cameraParams, T2_atris, LV);
% disp(az_angle);
% disp(el_angle);

% hold on;
% plot(centers2D(:,1), centers2D(:,2),'g+');
% for i = 1:size(centers2D,1)
%     hold on;
%     text(centers2D(i,1)+1, centers2D(i,2)+1,num2str(i),'Color','g');
% end

end

function [centers3D, az_angle, el_angle] = c2a(centers2D, cameraParams, T2_atris, LV)
    centers2D_tmp = calib.normalizePixel(centers2D', cameraParams);
    centers2D_tmp = centers2D_tmp';
    centers3D = [ones(size(centers2D_tmp,1),1), -centers2D_tmp(:,1), centers2D_tmp(:,2)]';
    
    centers3D = LV*T2_atris*centers3D;
    % centers3D = NPSK*(T2_inv_LV_inv{iCam})'*centers3D;
    
    x = centers3D(1,:);
    y = centers3D(2,:);
    z = centers3D(3,:);
    r = sqrt(x.^2 + y.^2 + z.^2);

    az_angle = atan2d(z(:),x(:));
    el_angle = asind(y(:)./r(:));

end


